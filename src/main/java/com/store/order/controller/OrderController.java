package com.store.order.controller;

import java.io.FileNotFoundException;
import java.io.FileOutputStream;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.itextpdf.text.Document;
import com.itextpdf.text.DocumentException;
import com.itextpdf.text.Paragraph;
import com.itextpdf.text.Phrase;
import com.itextpdf.text.pdf.PdfPCell;
import com.itextpdf.text.pdf.PdfPTable;
import com.itextpdf.text.pdf.PdfWriter;
import com.store.entity.vo.CartItems;
import com.store.order.entity.OrderItemsInfo;
import com.store.order.entity.OrderTable;
import com.store.order.service.OrderService;

@RestController
public class OrderController {

	@Autowired
	OrderService orderService;

	@RequestMapping(value = "/processingOrder/{emailId}", method = RequestMethod.POST, headers = "Accept=application/json")
	public void storeOrder(@RequestBody CartItems[] cart, @PathVariable("emailId") String emailId, ModelMap map)
			throws FileNotFoundException, DocumentException {
		
		OrderTable order = new OrderTable();
		order.setMailId(emailId);
		orderService.addOrder(order);
		for (CartItems cartItems : cart) {
			OrderItemsInfo items = new OrderItemsInfo();
			items.setOrder(order);
			items.setProductId(cartItems.getProductId());
			items.setQuantity(cartItems.getQuantity());
			orderService.addOrderItems(items);
		}

		Document document = new Document();
		PdfWriter.getInstance(document, new FileOutputStream("Order.pdf"));
		//com.itextpdf.text.List a = new com.itextpdf.text.List();
		double totalPrice = 0;
		document.open();
		
		document.add(new Paragraph("Your Coviam Store Invoice"));
		document.add(new Paragraph(" "));
		document.add(new Paragraph(" "));
		document.add(new Paragraph(" "));
		for (CartItems cartItems : cart) {
			totalPrice = totalPrice + (cartItems.getProductPrice() * cartItems.getQuantity());
		}
		
		PdfPTable table = new PdfPTable(4);
        // the cell object
        PdfPCell cell;
        // we add a cell with colspan 3
        
        // now we add a cell with rowspan 2
        cell = new PdfPCell(new Phrase("Product Name"));
        cell.setRowspan(1);
        table.addCell(cell);
        
        cell = new PdfPCell(new Phrase("Price"));
        cell.setRowspan(1);
        table.addCell(cell);
        
        cell = new PdfPCell(new Phrase("Quantity Ordered"));
        cell.setRowspan(1);
        table.addCell(cell);
        
        cell = new PdfPCell(new Phrase("Total Price of Product "));
        cell.setRowspan(1);
        table.addCell(cell);
        // we add the four remaining cells with addCell()
       
        
		for (CartItems cartItems : cart) {
			table.addCell(cartItems.getProductName());
			table.addCell("Rs. " + Double.toString(cartItems.getProductPrice()));
			table.addCell(Integer.toString(cartItems.getQuantity()));
			table.addCell("Rs. " + Double.toString(cartItems.getProductPrice()*cartItems.getQuantity()));
//			a.add("Product: " + cartItems.getProductName());
//			a.add("Price: " + Double.toString(cartItems.getProductPrice()));
//			a.add("Quantity of " + cartItems.getProductName() + " " + Integer.toString(cartItems.getQuantity()));

		}
		
		//table.addCell("Total Price: " + Double.toString(totalPrice));
		
		
		
		document.add(table);
		//document.add(a);
		document.add(new Paragraph(" "));
		document.add(new Paragraph(" "));
		document.add(new Paragraph("Total Ampunt Paid: Rs. " + totalPrice + " /-"));
		document.close();

		orderService.sendNotification(emailId + ".com");

	}

	@RequestMapping(value = "/sendMail/{emailId}", method = RequestMethod.POST, headers = "Accept=application/json")
	public boolean sendMail(@RequestBody CartItems[] cart, @PathVariable("emailId") String emailId)
			throws FileNotFoundException, DocumentException {

		return true;
	}

}
package com.store.order.dao;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import com.store.order.entity.OrderTable;
import com.store.order.entity.OrderItemsInfo;




@Repository
public interface OrderItemsDAO extends CrudRepository<OrderItemsInfo, Integer>{

}
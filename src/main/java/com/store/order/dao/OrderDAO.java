package com.store.order.dao;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import com.store.order.entity.OrderTable;

@Repository
public interface OrderDAO extends CrudRepository<OrderTable, Integer>{

}

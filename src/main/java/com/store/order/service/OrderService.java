package com.store.order.service;

import com.store.order.entity.OrderTable;
import com.store.order.entity.OrderItemsInfo;

public interface OrderService {
	

	void addOrderItems(OrderItemsInfo order);
	
	void addOrder(OrderTable order);
	
	void sendNotification(String emailId);

}

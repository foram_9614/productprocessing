package com.store.order.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.mail.SimpleMailMessage;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.store.order.dao.OrderDAO;
import com.store.order.dao.OrderItemsDAO;
import com.store.order.entity.OrderTable;
import com.store.order.entity.OrderItemsInfo;

@Service
@Transactional
public class IOrderService implements OrderService {

	@Autowired
	OrderItemsDAO orderItemsDAO;
	
	@Autowired
	OrderDAO orderDAO;

	private JavaMailSender javaMailSender;
	public IOrderService(JavaMailSender javaMailSender) {
		this.javaMailSender = javaMailSender;
		// TODO Auto-generated constructor stub
	}
	@Override
	public void addOrderItems(OrderItemsInfo order) {
		// TODO Auto-generated method stub
		orderItemsDAO.save(order);
	}

	@Override
	public void addOrder(OrderTable order) {
		// TODO Auto-generated method stub
		orderDAO.save(order);
	}

	@Override
	public void sendNotification(String emailId) {
		System.out.println(emailId);
		SimpleMailMessage mail = new SimpleMailMessage();
		mail.setTo(emailId);
		mail.setFrom("aishwary.varshney@coviam.com");
		mail.setSubject("Order Successfully Placed");
		mail.setText("Your order has been placed successfully");
		
		javaMailSender.send(mail);
		
		
	}
	
	
	


}

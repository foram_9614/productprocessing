package com.store.order.entity;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity
@Table(name ="order_details")
public class OrderItemsInfo implements Serializable{
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	@Id
	@GeneratedValue
	@Column(name="order_item_id")
	private int orderdetailsId;
	
	@ManyToOne
	@JoinColumn(name = "order_id")
	private OrderTable order;
	
	@Column
	int productId;
	
	@Column
	int quantity;

	

	public int getOrderdetailsId() {
		return orderdetailsId;
	}

	public void setOrderdetailsId(int orderdetailsId) {
		this.orderdetailsId = orderdetailsId;
	}

	public int getProductId() {
		return productId;
	}

	public void setProductId(int productId) {
		this.productId = productId;
	}

	public int getQuantity() {
		return quantity;
	}

	public void setQuantity(int quantity) {
		this.quantity = quantity;
	}

	
	public OrderTable getOrder() {
		return order;
	}

	public void setOrder(OrderTable order) {
		this.order = order;
	}

	@Override
	public String toString() {
		return "OrderItemsInfo [orderdetailsId=" + orderdetailsId + ", order=" + order + ", productId=" + productId
				+ ", quantity=" + quantity + "]";
	}

	
	
	

}

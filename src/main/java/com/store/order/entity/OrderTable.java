package com.store.order.entity;

import java.io.Serializable;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;



@Entity
@Table (name = "order_table")
public class OrderTable implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "order_seq")
	@SequenceGenerator(name = "order_seq", sequenceName = "orderid_sequence")
	@Column(name="order_id")
	int orderID;

	@Column
	String mailId;


	@OneToMany(mappedBy= "order", cascade = CascadeType.ALL, fetch= FetchType.EAGER)
	List<OrderItemsInfo> orderList;

	public int getOrderID() {
		return orderID;
	}

	public void setOrderID(int orderID) {
		this.orderID = orderID;
	}

	public String getMailId() {
		return mailId;
	}

	public void setMailId(String mailId) {
		this.mailId = mailId;
	}

	public List<OrderItemsInfo> getOrderList() {
		return orderList;
	}

	public void setOrderList(List<OrderItemsInfo> orderList) {
		this.orderList =  orderList;
	}

	@Override
	public String toString() {
		return "Order [orderID=" + orderID + ", mailId=" + mailId + ", orderList=" + orderList + "]";
	}


}
